from fastapi import FastAPI
from router import vacations


app = FastAPI()
app.include_router(vacations.router)
